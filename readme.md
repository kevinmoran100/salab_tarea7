# Docker

Dentro de esta carpeta se encuentra la configuracion necesaria para poder levantar un servidor web con node js y una base de datos con MYSQL.

## dockerfile

Tiene la configuracion para conectar con MYSQL de:
- db name
- user name
- user pass
- host
- port


## Como usarlo

Construir los contenedores con el siguiente comando: docker-compose up

## Video de demostración

https://youtu.be/j4XvXUgmAfg


