CREATE TABLE cadena (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  cadena VARCHAR(250) NOT NULL
);

insert into cadena (cadena) values ('Esta es una cadena modificada y es la cadena no. 1');
insert into cadena (cadena) values ('Esta es una cadena y es la cadena no. 2');
insert into cadena (cadena) values ('Esta es una cadena modificada y es la cadena no. 3');
insert into cadena (cadena) values ('Esta es una cadena y es la cadena no. 4');
insert into cadena (cadena) values ('Esta es una cadena y es la cadena no. 6');
insert into cadena (cadena) values ('Esta es una cadena y es la cadena no. 7');
insert into cadena (cadena) values ('Esta es una cadena y es la cadena no. 8');
insert into cadena (cadena) values ('Esta es una cadena y es la cadena no. 9');