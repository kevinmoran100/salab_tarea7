FROM node:10
ENV DATABASE_HOST=db
ENV MYSQL_PORT 3306
ENV MYSQL_USER kevmo
ENV MYSQL_PASSWORD 1234
ENV MYSQL_DATABASE lab_sa
# Crear el directorio de la app
WORKDIR /usr/src/web
# instalar dependencias
COPY package*.json ./
RUN npm install
# copiar el codigo
COPY . .
EXPOSE 3001
# ip de la base de datos
## THE LIFE SAVER
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

## Launch the wait tool and then your application
CMD /wait && node index.js