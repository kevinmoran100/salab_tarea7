'use strict';
const express = require('express');
const app = express();
var mysql = require('mysql');

//conexion con mysql
const db = mysql.createConnection({
    host: process.env.DATABASE_HOST || '127.0.0.1',
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    port: process.env.MYSQL_PORT,
    database: process.env.MYSQL_DATABASE
});

db.connect(function (err) {
    if (err) throw err;
    console.log("Connected to MYSQL!");
});

// Establecer el engine de vistas
app.set("view engine", "pug");
// serve static files from the `public` folder para estilos
app.use(express.static(__dirname + "/public"));
// establecer puerto
app.listen(3001, () => console.log('escuchando...'));
// Ruta principal
app.get('/', (req, res) => {
    // Hacer consulta a la base de datos de las cadenas
    db.query("SELECT id,cadena FROM cadena;", (err, result) => {
        if (err) {
            // Error al hacer la consulta
            console.log(err);
            res.status(500).json({
                "estado": "500",
                "mensaje": "Error al conectar con la base de datos"
            });
        } else {
            // Consulta exitosa
            console.log(result);
            // Enviar la visualizacion de la información
            res.render("index", {
                title: "Tarea no. 7 - Laboratorio SA",
                arraycadenas: result
              });
        }
    });
});